# Convert MicroDrop v2.0 DropBot v2/v3 device to MR-Box v2 device mapping #

Motivation:

 - The **DropBot v2** and **DropBot v3** systems share the same **channel
   number to pogo pin location mapping**.
     * i.e., MicroDrop DMF device files are compatible between **DropBot v2**
       and **DropBot v3**
 - The **MR-Box v2** channel number to pogo pin location mapping **is
   different** the mapping for the **DropBot v2/v3 systems**.
     * The channel order is completely reversed, e.g., DropBot v2/v3 channel
       0 is MR-Box v2 channel 119, DropBot v2/v3 channel 119 is MR-Box v2
       channel 0.

This tool translates **DropBot v2/v3** MicroDrop device files to work on
**MR-Box v2**.

# Usage #

Run the following command from within this directory:

    jupyter notebook "MicroDrop v2.0 - DropBot v2-v3 to MR-Box v2 device converter.ipynb"
