import cStringIO as StringIO

import lxml
import microdrop as md
import microdrop.dmf_device


def translate_device_v2_to_mrbox_v2(v2_device_filepath):
    '''
    Convert MicroDrop v2.0 DropBot v2/v3 device to MR-Box v2 device mapping.

    Parameters
    ----------
    v2_device_filepath : str
        Path to MicroDrop v2.0 DropBot v2/v3 SVG device file.

    Returns
    -------
    lxml.Etree
        Root of modified XML device document.

        Use ``.write(filepath)`` method to write result to file.
    '''
    # Load MicroDrop v2.0 device.
    device = md.dmf_device.DmfDevice.load(v2_device_filepath)

    # Load SVG content into file-like buffer.
    device_io = StringIO.StringIO(device.to_svg())

    # Parse SVG xml document.
    root = lxml.etree.parse(device_io)

    # Find all electrodes in device XML using XPath query.
    electrodes = root.xpath("//svg:g[@inkscape:label='Device']"
                            "/svg:path[@data-channels]",
                            namespaces=md.dmf_device.INKSCAPE_NSMAP)
    # Map each integer channel to corresponding electrode SVG element.
    electrodes_by_channel = {int(electrode_i.attrib['data-channels']):
                             electrode_i for electrode_i in electrodes}

    # Get sorted list of channel numbers.
    channels = sorted(electrodes_by_channel.keys())

    # Construct mapping of channels in MR-Box v2 channel order to electrode
    # elements.
    reversed_channels = reversed(channels)
    reversed_electrodes_by_channel = \
        dict(zip(reversed_channels, [electrodes_by_channel[channel_i]
                                     for channel_i in channels]))

    # Update `data-channels` attribute of each electrode element in SVG XML
    # with MR-Box v2 channel.
    for channel_i, electrode_i in reversed_electrodes_by_channel.iteritems():
        electrode_i.attrib['data-channels'] = str(channel_i)

    return root
