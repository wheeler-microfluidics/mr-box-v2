import logging
import os

import gtk
import path_helpers as ph
from dropbot_v3_to_mr_box_v2 import translate_device_v2_to_mrbox_v2
import ipywidgets as ipw
import mpm
import mpm.commands


logger = logging.getLogger(__name__)


def _device_svg_filter():
    # Only show SVG files.
    filter = gtk.FileFilter()
    filter.set_name("MicroDrop device files (*.svg)")
    filter.add_mime_type("image/svg+xml")
    filter.add_pattern("*.svg")
    return filter


def select_filename(dialog=None, default_folder=None,
                    default_name=None):
    if default_folder is None:
        default_folder = ph.path(os.getcwd()).realpath()

    if dialog is None:
        dialog = gtk.FileChooserDialog('Save file', None,
                                       gtk.FILE_CHOOSER_ACTION_SAVE,
                                       (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                        gtk.STOCK_SAVE, gtk.RESPONSE_OK))

    dialog.add_filter(_device_svg_filter())

    # Position dialog under mouse pointer.
    dialog.set_position(gtk.WIN_POS_MOUSE)

    dialog.set_current_folder(default_folder)
    if default_name:
        dialog.set_current_name(default_name)
    response = dialog.run()

    if response == gtk.RESPONSE_OK:
        filepath = ph.path(dialog.get_filename()).realpath()
    else:
        filepath = None
    dialog.destroy()
    return filepath


def select_save_filepath(**kwargs):
    '''
    Prompt user to select an output filepath to save to.

    If selected filepath already exists, prompt user to overwrite.  If
    user chooses not to overwrite, prompt again for another path.

    Returns
    -------
    str or None
        Selected filepath or ``None`` if user presses ``Cancel`` button.
    '''
    while True:
        filepath = select_filename(**kwargs)
        if filepath is None:
            # User cancelled dialog.
            return None

        if not filepath.exists():
            break
        else:
            overwrite_dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL,
                                                 gtk.MESSAGE_QUESTION,
                                                 gtk.BUTTONS_YES_NO,
                                                 'File `{}` already '
                                                 'exists. Overwrite?'
                                                 .format(filepath))
            overwrite_dialog.set_position(gtk.WIN_POS_MOUSE)
            response = overwrite_dialog.run()
            try:
                if response == gtk.RESPONSE_YES:
                    logger.warning('Overwrite %s.', filepath)
                    break
            finally:
                overwrite_dialog.destroy()
    return filepath


class DropBotv3ToMRBoxv2DeviceConverterUI(object):
    def __init__(self, default_folder=None):
        if default_folder is None:
            default_folder = (mpm.commands.get_plugins_directory().parent
                              .joinpath('devices'))
        def _input_device_file_browse(*args):
            dialog = gtk.FileChooserDialog('Select MicroDrop v2.0 device file', None,
                                           gtk.FILE_CHOOSER_ACTION_OPEN,
                                           (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                            gtk.STOCK_OPEN, gtk.RESPONSE_OK))
            filepath = select_filename(default_folder=default_folder,
                                       dialog=dialog)
            if filepath is not None:
                input_device_file.value = filepath

        input_device_file = ipw.Text(description='DropBot v2.0 device file:')
        input_device_file_browse = ipw.Button(description='Browse...')

        input_device_file_browse.on_click(_input_device_file_browse)

        def _translate_device(*args):
            input_filepath = ph.path(input_device_file.value)

            if not input_filepath:
                print 'Please select an input device file to translate.'
            elif not input_filepath.isfile():
                print ('Input file `%s` not found.  Please check path.' %
                       input_filepath)
            else:
                mrbox_v2_xml_root = translate_device_v2_to_mrbox_v2(input_filepath)
                mrbox_v2_filepath = \
                    select_save_filepath(default_folder=input_filepath.parent,
                                         default_name='device-mrbox_v2.svg')
                if mrbox_v2_filepath is not None:
                    mrbox_v2_xml_root.write(mrbox_v2_filepath)
        translate_device = ipw.Button(description='Translate device')
        translate_device.on_click(_translate_device)

        self.widget = ipw.VBox([ipw.HBox([input_device_file,
                                          input_device_file_browse]),
                                translate_device])
