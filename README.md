# MR-Box v2

A point-of-care diagnostic instrument designed to detect Measles and
Rubella antibodies in a drop of blood using a magnetic bead-based ELISA.
This system is being developed in the [Wheeler Microfluidics Lab]
(http://microfluidics.utoronto.ca) at the University of Toronto and is based
on the open-source [DropBot](http://sci-bots/dropbot) system.


For more information, see the [wiki](https://gitlab.com/wheeler-microfluidics/mr-box-v2/wikis/home).